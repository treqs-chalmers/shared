# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following text describes high level system requirements

### [requirement id=SR0021]

The system shall support creating invitations to participants.

### [requirement id=SR0021 quality=QR0003]

The system shall support creating a meeting event in a personal calendar.

### [requirement id=SR0022]

The system shall support event labeling and viewing 

### [requirement id=SR0023]

The system shall allow scheduling of resources
