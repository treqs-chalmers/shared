# High level quality requirements

## [quality id=QR0003]

The meeting scheduler system has the ability to handle explicit dependencies between meeting date and meeting location. Then again, it manages participation through “delegation”.
